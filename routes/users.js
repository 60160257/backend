const express = require('express');
const userController = require('../controller/UsersController');
const router = express.Router();
const usersController = require('../controller/UsersController')

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.json(usersController.getUsers());
});

router.get('/:id', (req, res, next) => {
  const { id } = req.params
  res.json(usersController.getUser(id))
})

router.post('/', (req, res, next) => {
  const payload = req.body
  res.json(usersController.addUser(payload))
})

router.put('/', (req, res, next) => {
  const payload = req.body
  res.json(usersController.updateUser(payload))
})

router.delete('/:id', (req, res, next) => {
  const { id } = req.params
  res.json(usersController.deleteUser(id))
})

module.exports = router;
